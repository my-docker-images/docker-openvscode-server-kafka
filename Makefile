
REPO = gitlab-research.centralesupelec.fr:4567/my-docker-images/docker-openvscode-server-kafka
TAG  = amd64

all:
	docker buildx create --use --node new-builder
	docker buildx build --push --platform "linux/amd64" --tag "${REPO}:${TAG}" .

