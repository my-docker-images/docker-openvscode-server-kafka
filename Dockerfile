
FROM gitlab-research.centralesupelec.fr:4567/my-docker-images/docker-openvscode-server:amd64

## software needed

ARG DEBIAN_FRONTEND="noninteractive"

RUN                                                \
     apt-get update --yes                          \
  && apt-get install -y --no-install-recommends    \
        # Maven package is quite old (3.6.3)
        # maven                                    \
        openjdk-17-jdk                             \
        # scp for transferring files
        ssh                                        \
  && apt-get clean                                 \
  && rm -rf                                        \
       /tmp/*                                      \
       /var/lib/apt/lists/*                        \
       /var/tmp/*

ARG MAVEN_RELEASE="3.9.6"
RUN                                                                                   \
     curl -o /tmp/apache-maven-3.9.6-bin.tar.gz -L                                     \
        "https://dlcdn.apache.org/maven/maven-3/${MAVEN_RELEASE}/binaries/apache-maven-${MAVEN_RELEASE}-bin.tar.gz" \
  && tar zxf /tmp/apache-maven-${MAVEN_RELEASE}-bin.tar.gz --directory=/opt           \
  && rm /tmp/apache-maven-${MAVEN_RELEASE}-bin.tar.gz                                 \
  && mv /opt/apache-maven-${MAVEN_RELEASE} /opt/maven                                 \
  && echo "export JAVA_HOME=/usr/lib/jvm/java-17-openjdk-amd64" >>/init-config/.zshrc \
  && echo "export PATH=/opt/maven/bin:\$PATH" >>/init-config/.zshrc


ARG SCALA_RELEASE="2.13"
ARG KAFKA_RELEASE="3.7.0"
RUN                                                                            \
    curl -o /tmp/kafka_${SCALA_RELEASE}-${KAFKA_RELEASE}.tgz                   \
       "https://dlcdn.apache.org/kafka/${KAFKA_RELEASE}/kafka_${SCALA_RELEASE}-${KAFKA_RELEASE}.tgz" \
  && tar zxf /tmp/kafka_${SCALA_RELEASE}-${KAFKA_RELEASE}.tgz --directory=/opt \
  && rm /tmp/kafka_${SCALA_RELEASE}-${KAFKA_RELEASE}.tgz                       \
  && mv /opt/kafka_${SCALA_RELEASE}-${KAFKA_RELEASE} /opt/kafka                \
  && chown -R abc:abc /opt/kafka                                               \
  && echo "export PATH=/opt/kafka/bin:\$PATH" >>/init-config/.zshrc            \
  && echo "listeners=PLAINTEXT://localhost:9092" >>/opt/kafka/config/server.properties

# Java extension for VS Code
RUN                                                                 \
    /app/openvscode-server/bin/openvscode-server                    \
        --extensions-dir /init-config/.openvscode-server/extensions \
        --install-extension vscjava.vscode-java-pack

COPY settings.json /init-config/.openvscode-server/data/Machine/
COPY if4030.kafka /init-config/workspace

