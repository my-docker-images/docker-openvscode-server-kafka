# docker-openvscode-server-3if4030

## What

This image is based on [docker-openvscode-server-cpp](https://gitlab-research.centralesupelec.fr/my-docker-images/docker-openvscode-server-cpp),
it adds some needed tools for the Kafka exercise of the course 3IF4030.

Included tools are:
- maven
- kafka
- openjdk-17-jdk

The initial project is also included.

## Details

- The exposed port is 3000
- The user folder is `/config`
- the user and sudo password is `abc`
- if docker is installed on your computer, you can run (amd64 architecture) this 
  image, assuming you are in a specific folder that will be shared with the container at 
  `/config`, with:
  
  `docker run -p 3000:3000 -v "$(pwd):/config"
    gitlab-research.centralesupelec.fr:4567/my-docker-images/docker-openvscode-server-kafka`

